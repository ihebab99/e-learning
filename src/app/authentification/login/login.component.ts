import { Component, OnInit } from '@angular/core';
import { LoginRequest } from 'src/app/model/loginRequest';
import { AuthService } from 'src/app/shared/auth.service';
import { FormsModule } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  implements OnInit {
  loginRequest : LoginRequest={
    email:'',
    password:''
  }
  loginError: string = '';


  constructor(private authService: AuthService) { }
 
 
  ngOnInit(): void {
   
     }

     login(): void {
      this.authService.login(this.loginRequest).subscribe(
        () => {
          // Login successful
          console.log('Login successful');
          // Optionally, you can redirect the user to a different page or perform other actions
        },
        error => {
          // Login failed
          console.error('Login failed:', error);
          this.loginError = 'Invalid email or password. Please try again.'; // Display error message on the form
        }
      );
    }


    }
