import { Component } from '@angular/core';
import { Role } from 'src/app/model/enum/role.enum';
import { RegisterRequest } from 'src/app/model/registerRequest';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registerRequest:RegisterRequest = new RegisterRequest('','','','',Role.ADMIN);
  registrationError: string = '';

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }
  register(): void {
    this.authService.register(this.registerRequest).subscribe(
      () => {
        // Registration successful
        console.log('Registration successful');
        // Optionally, you can redirect the user to a success page or perform other actions
      },
      error => {
        // Registration failed
        console.error('Registration failed:', error);
        this.registrationError = 'Registration failed. Please try again.'; // Display error message on the form
      }
    );
  }
  
}
