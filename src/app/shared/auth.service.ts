import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoginRequest } from '../model/loginRequest';
import { Observable, tap } from 'rxjs';
import { RegisterRequest } from '../model/registerRequest';
import { AuthenticationResponse } from '../model/authentication.Response';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public  BASEURL = environment.apiUrl;
  private accessTokenKey = 'access_token';
  private refreshTokenKey = 'refresh_token';


  constructor(private http : HttpClient) { }

  // login(login:LoginRequest) : Observable<AuthenticationResponse>{
  //   return this.http.post<AuthenticationResponse>(`${this.BASEURL}/login`,login);
  // }

  login(request: LoginRequest): Observable<AuthenticationResponse> {
    return this.http.post<AuthenticationResponse>(`${this.BASEURL}/login`, request)
      .pipe(
        tap(response => this.storeTokens(response))
      );
  }


  register(register:RegisterRequest) : Observable<AuthenticationResponse>{
    return this.http.post<AuthenticationResponse>(`${this.BASEURL}/register`, register);
  }

  // refreshToken(): Observable<any> {
  //   return this.http.post(`${this.BASEURL}/refresh-token`, {});
  // }


  refreshToken(): Observable<AuthenticationResponse> {
    const refreshToken = this.getRefreshToken();
    return this.http.post<AuthenticationResponse>(`${this.BASEURL}/refresh-token`, {})
      .pipe(
        tap(response => this.storeAccessToken(response.access_token))
      );
  }

  storeTokens(response: AuthenticationResponse): void {
    this.storeAccessToken(response.access_token);
    this.storeRefreshToken(response.refresh_token);
  }

  storeAccessToken(token: string): void {
    localStorage.setItem(this.accessTokenKey, token);
  }

  getAccessToken(): string | null {
    return localStorage.getItem(this.accessTokenKey);
  }

  storeRefreshToken(token: string): void {
    localStorage.setItem(this.refreshTokenKey, token);
  }

  getRefreshToken(): string | null {
    return localStorage.getItem(this.refreshTokenKey);
  }

  clearTokens(): void {
    localStorage.removeItem(this.accessTokenKey);
    localStorage.removeItem(this.refreshTokenKey);
  }






}
