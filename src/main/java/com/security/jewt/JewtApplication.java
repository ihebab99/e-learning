package com.security.jewt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JewtApplication {

    public static void main(String[] args) {
        SpringApplication.run(JewtApplication.class, args);
    }

}
