package com.security.jewt.model;

public enum Role {
    USER,
    ADMIN
}
