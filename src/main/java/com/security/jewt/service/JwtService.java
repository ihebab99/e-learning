package com.security.jewt.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtService {

    private static final String SECRET_KEY ="3VGU05HKNRXljDcCmaq9Ti/o6PtcYYAt3NTCEcyiEIkwC/uOBmHgoVPPcJtV4PItp9r+vhp6VXy5sIAhuYNEbZ79UlzoA0Gbi/Ok7pksHuCr179svqTw0+AbM/yypKF2BlZtHjfJq/DRhqrVtXp/IxRPvuVvjJ/fVAnT5OretgCNs4qkRQ5tV0BM5TOSW4imyorGX1w+TAMpUHQ6gVd5D1VApeNGLfcMnuAwTfiLF9Q8EIRjnAsB4EOqjg07LOKSzflBLOZwBfI8Uviigs6N8fcl3/EjlhQje+kBMO2g8KXJDsAOfWyPaBcHur6EJXD2nCD9uyd4nQaelOGyVggKEKd0GHlpfQEwxWr3boIBAUI=\n";

    /**
     * get the username which is the email from the claim
     * @param token
     * @return
     */
    public String extractUsername(String token) {
        // the subject of the token here is the email
        return extractClaim(token, Claims::getSubject);
    }


    /**
     * extract the content of a claim one by one
     * @param token
     * @param claimsTResolver
     * @return
     * @param <T>
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsTResolver){
        final Claims claims = extractAllClaims(token);
        return claimsTResolver.apply(claims);
    }

    public String generateToken(UserDetails userDetails){
        return generateToken(new HashMap<>(), userDetails);
    }

    /**
     * here we generated a token from the extra claims by giving the subject which is username and then, we set the expiration date for the token
     * @param extraClaims
     * @param userDetails
     * @return
     */
    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails){
        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24))
                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean isTokenValid(String token , UserDetails userDetails){
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }


    private Claims extractAllClaims(String token){
        return Jwts.
                parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * decode the secret key and get its content
     * @return
     */
    private Key getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
